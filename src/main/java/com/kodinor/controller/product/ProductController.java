package com.kodinor.controller.product;

import com.kodinor.entity.product.Product;
import com.kodinor.services.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/product")
    @ResponseStatus(code = HttpStatus.OK)
    public List<Product> getAllProduct(){
        return productService.getAllProduct();
    }

    @PostMapping("/product")
    @ResponseStatus(code = HttpStatus.OK)
    public Product saveProduct(@RequestBody Product product){
        return productService.saveProduct(product);
    }
}
