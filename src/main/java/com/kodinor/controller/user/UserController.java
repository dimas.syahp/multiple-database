package com.kodinor.controller.user;

import com.kodinor.entity.product.Product;
import com.kodinor.entity.user.User;
import com.kodinor.services.product.ProductService;
import com.kodinor.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/user")
    @ResponseStatus(code = HttpStatus.OK)
    public List<User> getAllUser(){
        return userService.getAllUser();
    }

    @PostMapping("/user")
    @ResponseStatus(code = HttpStatus.OK)
    public User saveUser(@RequestBody User user){
        return userService.saveUser(user);
    }
}
