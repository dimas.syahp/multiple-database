package com.kodinor.configuration;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * @author Ramesh Fadatare
 *
 */
@Configuration
@EnableJpaRepositories(
		basePackages = "com.kodinor.repository.user",
		entityManagerFactoryRef = "oracleEntityManagerFactory",
		transactionManagerRef = "oracleTransactionManager"
)
public class UserDBConfiguration
{
	@Autowired
	private Environment env;

	@Primary
	@Bean
	@ConfigurationProperties(prefix="spring.datasource1")
	public DataSourceProperties oracleDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Primary
	@Bean
	public DataSource oracleDataSource() {
		DataSourceProperties oracleDataSourceProperties = oracleDataSourceProperties();
		return DataSourceBuilder.create()
				.driverClassName(oracleDataSourceProperties.getDriverClassName())
				.url(oracleDataSourceProperties.getUrl())
				.username(oracleDataSourceProperties.getUsername())
				.password(oracleDataSourceProperties.getPassword())
				.build();
	}

	@Primary
	@Bean
	public PlatformTransactionManager oracleTransactionManager()
	{
		EntityManagerFactory factory = oracleEntityManagerFactory().getObject();
		return new JpaTransactionManager(factory);
	}

	@Primary
	@Bean
	public LocalContainerEntityManagerFactoryBean oracleEntityManagerFactory()
	{
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setDataSource(oracleDataSource());
		factory.setPackagesToScan(new String[]{"com.kodinor.entity.user"});
		factory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

		Properties jpaProperties = new Properties();
		jpaProperties.put("hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));
		jpaProperties.put("hibernate.show-sql", env.getProperty("spring.jpa.show-sql"));
		factory.setJpaProperties(jpaProperties);

		return factory;
	}
}
