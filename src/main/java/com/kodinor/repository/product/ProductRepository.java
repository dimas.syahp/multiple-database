package com.kodinor.repository.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.kodinor.entity.product.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
	//
}
