package com.kodinor.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.kodinor.entity.user.User;

public interface UserRepository extends JpaRepository<User, Long> {
	//
}
