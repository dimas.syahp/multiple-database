package com.kodinor.services.product;

import com.kodinor.entity.product.Product;

import java.util.List;

public interface ProductService {

    List<Product> getAllProduct();

    Product saveProduct(Product product);
}
