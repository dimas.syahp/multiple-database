package com.kodinor.services.user;

import com.kodinor.entity.user.User;

import java.util.List;

public interface UserService {

    List<User> getAllUser();

    User saveUser(User user);
}
